﻿using KatasAugust2022._04_BriefcaseLock;
using KatasAugust2022._08_CelciusToFahrenheit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace KatasAugust2022Tests
{
    public class CalciusToFahrenheitTests
    {
        [Fact]
        public void Convert_35Celcius_ShouldReturn95Fahrenheit()
        {
            // Arrange
            string expected = "95*F";

            // Act 

            string actual = CelciusToFahrenheit.Convert("35*C");

            // Assert 

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Convert_19Fahrenheit_ShouldReturnMinus7Celcius()
        {
            // Arrange
            string expected = "-7*C";

            // Act 

            string actual = CelciusToFahrenheit.Convert("19*F");

            // Assert 

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Convert_33_ShouldReturnErrorMessage()
        {
            // Arrange
            string expected = "Error";

            // Act 

            string actual = CelciusToFahrenheit.Convert("33");

            // Assert 

            Assert.Equal(expected, actual);
        }
    }
}

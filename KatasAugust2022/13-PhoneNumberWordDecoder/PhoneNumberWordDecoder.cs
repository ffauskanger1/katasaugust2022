﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace KatasAugust2022._13_PhoneNumberWordDecoder
{
    public class PhoneNumberWordDecoder
    {
        public static string TextToNum(string phonenumber)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in phonenumber)
            {
                sb.Append(Encode(c.ToString()));
            }
            return sb.ToString();
        }

        // Would have to redo the way I did this to do part 2 probably
        private static string Encode(string input)
        {
            if(Regex.IsMatch(input, "[A-Ca-c]"))
                return "2";
            else if (Regex.IsMatch(input, "[D-Fd-f]"))
                return "3";
            else if (Regex.IsMatch(input, "[G-Ig-i]"))
                return "4";
            else if (Regex.IsMatch(input, "[J-Lj-l]"))
                return "5";
            else if (Regex.IsMatch(input, "[M-Om-o]"))
                return "6";
            else if (Regex.IsMatch(input, "[P-Sp-s]"))
                return "7";
            else if (Regex.IsMatch(input, "[T-Vt-v]"))
                return "8";
            else if (Regex.IsMatch(input, "[W-Zw-z]"))
                return "9";
            else
                return input;
        }

        
        private static string Decode(string input)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatasAugust2022._06_PigLatin
{
    public static class PigLatin
    {
        public static char[] vowels = new char[] { 'a', 'e', 'i', 'o', 'u' };
        public static string translateWord(string word)
        {
            // Empty check
            if (word == "")
                return "";

            if (!vowels.Contains(char.ToLower(word[0]))) // Consonant
            {
                int firstVowel = word.IndexOfAny(vowels, 1);
                return firstVowel != -1 ? $"{word.Substring(firstVowel)}{word.Substring(0, firstVowel)}ay" : $"{word.Reverse().ToString()}ay";
            }
            return $"{word}yay"; // Vowel
        }

        public static string translateSentence(string sentence)
        {
            string[] splitSentence = sentence.Split(" ");
            string newSentence = "";
            foreach(string word in splitSentence)
            {
                newSentence += translateWord(word) + " ";
            }
            newSentence = char.ToUpper(newSentence.First()) + newSentence.Substring(1).ToLower();
            return newSentence;
        }

    }
}

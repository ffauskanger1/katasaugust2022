﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatasAugust2022._01_Parseltounge
{
    public static class Parseltounge
    {
        public static bool IsParselTounge(string phrase)
        {
            bool isParselTounge = true;
            string[] words = phrase.Split(' ');

            foreach(string word in words)
            {
                string lowerCaseWord = word.ToLower();
                int count = lowerCaseWord.Count(c => c == 's');
                if (!(count == 0 || lowerCaseWord.Contains("ss")))
                {
                    isParselTounge = false;
                }
            }

            return isParselTounge;
        }
    }
}

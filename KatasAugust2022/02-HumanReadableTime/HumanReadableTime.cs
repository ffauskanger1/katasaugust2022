﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatasAugust2022._0_2HumanReadableTime
{
    public static class HumanReadableTime
    {
        public static string GetReadableTime(uint seconds)
        {
            //uint hours = 0;
            //uint minutes = 0;
            //if (seconds >= 3600)
            //{
            //    hours = seconds / 3600;
            //    seconds -= hours * 3600;
            //}
            //if (seconds >= 60)
            //{
            //    minutes = seconds / 60;
            //    seconds -= minutes * 60;
            //}

            uint hours = seconds / 3600;
            uint minutes = (seconds / 60) % 60;
            seconds %= 60;

            return $"{hours:00}:{minutes:00}:{seconds:00}"; ;
        }
    }
}

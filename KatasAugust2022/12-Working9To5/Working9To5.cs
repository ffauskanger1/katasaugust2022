﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace KatasAugust2022._12_Working9To5
{
    public class Working9To5
    {
        // Calulates the total pay and handles errors with length
        public static string Overtime(decimal[] calculatePayArray) => calculatePayArray.Length == 4 ? $"${CalculateOvertime(calculatePayArray).ToString("0.00")}" : "Error";
        
        // Calculates overtime hours (overtimepay (overtimehours * rate * multiplier) + normal pay (endhour-starthour-overtimehours * rate))
        private static decimal CalculateOvertime(decimal[] calculatePayArray) => CalculateOvertimeHours(calculatePayArray[1]) * calculatePayArray[2] * calculatePayArray[3] + 
            (calculatePayArray[1] - calculatePayArray[0] - CalculateOvertimeHours(calculatePayArray[1])) * calculatePayArray[2];
        
        // Gets the total hours of overtime hours
        private static decimal CalculateOvertimeHours(decimal hours) => hours > 17 ? hours - 17 : 0;
    }
}

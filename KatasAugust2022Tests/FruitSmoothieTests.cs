﻿using KatasAugust2022._05_FruitSmoothie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Sdk;

namespace KatasAugust2022Tests
{
    public class FruitSmoothieTests
    {
        [Fact]
        public void Ingredients_ContainsBananaAndSoy_ShouldReturnBananaAndSoy()
        {
            // Arrange
            FruitSmoothie s1 = new FruitSmoothie(new string[] { "Banana", "Soy" });

            string[] expected = new string[] { "Banana", "Soy" };

            // Act 

            string[] actual = s1.Ingredients;

            // Assert 

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void GetCost_BananaAndSoySmoothie_ShouldCostZeroFifty()
        {
            // Arrange
            FruitSmoothie s1 = new FruitSmoothie(new string[] { "Banana", "Soy" });

            string expected = "$0,5";

            // Act 

            string actual = s1.GetCost();

            // Assert 

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void GetPrice_BananaAndSoySmoothie_ShouldCostOneTwentyFive()
        {
            // Arrange
            FruitSmoothie s1 = new FruitSmoothie(new string[] { "Banana", "Soy" });
            string expected = "$1,25";

            // Act 

            string actual = s1.GetPrice();

            // Assert 

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void MinTurns_Code4089ShortestTurns_ShouldReturnCorrectSum()
        {
            // Arrange
            FruitSmoothie s1 = new FruitSmoothie(new string[] { "Banana", "Soy" });
            string expected = "Banana Smoothie";

            // Act 

            string actual = s1.GetName();

            // Assert 

            Assert.Equal(expected, actual);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace KatasAugust2022._08_CelciusToFahrenheit
{
    public static class CelciusToFahrenheit
    {

        public static string Convert(string celciusOrFahrenheit)
        {
            string result = "Error";
            try
            {
                string[] splitString = celciusOrFahrenheit.Split("*");
                if (splitString.Length != 2)
                    return result;

                double temperature = double.Parse(splitString[0]);
                string type = splitString[1];

                if (type == "F")
                {
                    double calculation = Math.Round((temperature - 32) / (1.8));
                    result = $"{calculation}*C";
                }

                if (type == "C")
                {
                    double calculation = Math.Round((temperature * 1.8) + 32);
                    result = $"{calculation}*F";
                }
            }
            catch (Exception)
            {
                return result;
            }

            return result;
        }

        public static string ConvertWithRegex(string celciusOrFahrenheit)
        {
            string result = "Error";
            if(Regex.IsMatch(celciusOrFahrenheit, @"^([-]?\d{1,10}\*[CF])$"))
            {
                string[] splitString = celciusOrFahrenheit.Split("*");

                double temperature = double.Parse(splitString[0]);
                string type = splitString[1];

                if (type == "F")
                {
                    double calculation = Math.Round((temperature - 32) / (1.8));
                    result = $"{calculation}*C";
                }

                if (type == "C")
                {
                    double calculation = Math.Round((temperature * 1.8) + 32);
                    result = $"{calculation}*F";
                }
            }
            return result;
        }
    }
}

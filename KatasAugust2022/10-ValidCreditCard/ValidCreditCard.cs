﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatasAugust2022._10_ValidCreditCard
{
    public class ValidCreditCard
    {
        public static bool isValidCreditCard(long number)
        {
            if (number.ToString().Length > 19 || number.ToString().Length < 14)
                return false;
            
            List<int> listofNumbers = number.ToString().Select(value => int.Parse(value.ToString())).ToList();
            
            // Keep checkdigit
            int checkDigit = listofNumbers.ElementAt(listofNumbers.Count - 1);

            // Remove checkdigit
            listofNumbers.RemoveAt(listofNumbers.Count - 1);

            // Reverse list
            listofNumbers.Reverse();
            

            // Adding *2 to odd numbers 
            for(int i = 0; i < listofNumbers.Count; i++)
            {
                if(i % 2 == 0)
                {
                    listofNumbers[i] *= 2;
                    if (listofNumbers[i] > 9)
                    {
                        // Adding first and second digit
                        listofNumbers[i] = (listofNumbers[i] / 10) % 10 + listofNumbers[i] % 10;
                    }
                }
            }
            // Calulating sum
            int sum = listofNumbers.Sum(x => x);
            // Checking last digit towards checkdigit
            return (10 - ((sum % 10))) == checkDigit;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatasAugust2022._05_FruitSmoothie
{
    public class FruitSmoothie : IFruitSmoothie
    {
        public string[] Ingredients { get; set; }

        public FruitSmoothie(string[] ingredients)
        {
            Ingredients = ingredients;
        }

        public string GetCost()
        {
            double totalCost = 0;
            foreach(string ingredient in Ingredients)
            {
                double price = 0;
                Fruit.GetFruits().TryGetValue(ingredient, out price);
                totalCost += price;
            }

            return $"${totalCost.ToString()}";
        }

        public string GetName()
        {
            string name = string.Empty;
            List<string> sortedIngredients = Ingredients.ToList();
            sortedIngredients.Sort();

            int fruitCount = 0;
            foreach(string ingredient in sortedIngredients)
            {
                if (Fruit.GetFruits().ContainsKey(ingredient))
                {
                    string ingredientName = ingredient;
                    fruitCount++;
                    if (ingredient.Contains("berries"))
                        ingredientName = ingredient.Replace("berries", "berry"); // Doesn't work

                    name += $"{ingredientName} ";
                }
            }

            if (fruitCount > 1)
                name += "Fusion";
            else
                name += "Smoothie";
            return name;
        }

        public string GetPrice()
        {           
            double cost = Convert.ToDouble(GetCost().Split('$')[1]);
            double totalPrice = Math.Round(cost + cost * 1.5,2);
            return $"${totalPrice.ToString()}";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatasAugust2022._04_BriefcaseLock
{
    public static class BriefcaseLock
    {
        public static int MinTurns(string code, string target)
        {
            // Sets initial values
            int totalTurns = 0;
            int codeConverted = Convert.ToInt32(code);
            int targetConverted = Convert.ToInt32(target);

            for (int i = 0; i < code.Length; i++)
            {
                // Get digits
                int codeDigit = codeConverted % 10;
                int targetDigit = targetConverted % 10;

                // Calculate backward and forwards
                int turnsBackward = Math.Abs(codeDigit - targetDigit);
                int turnsForward = Math.Abs(turnsBackward - 10);

                // If it's longer with backward, use forwards
                if (turnsBackward > turnsForward)
                {
                    totalTurns += turnsForward;
                }
                else // Else use backwards (or 0 if they are equal).
                {
                    totalTurns += turnsBackward;
                }

                // Divide by 10, to prepare for next digit
                targetConverted /= 10;
                codeConverted /= 10;
            }
            
            return totalTurns;
        }
    }
}

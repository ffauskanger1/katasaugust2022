﻿using KatasAugust2022._0_2HumanReadableTime;
using KatasAugust2022._00_FizzBuzz;
using KatasAugust2022._01_Parseltounge;
using KatasAugust2022._03_RemoveTheWord;
using KatasAugust2022._04_BriefcaseLock;
using KatasAugust2022._05_FruitSmoothie;
using KatasAugust2022._06_PigLatin;
using KatasAugust2022._07_GaussAddition;
using KatasAugust2022._08_CelciusToFahrenheit;
using KatasAugust2022._09_SimplifyingFractions;
using KatasAugust2022._10_ValidCreditCard;
using KatasAugust2022._11_PalindromeTimestamps;
using KatasAugust2022._12_Working9To5;
using KatasAugust2022._13_PhoneNumberWordDecoder;
using System;
using System.Diagnostics;

namespace KatasAugust2022
{
    internal class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine(PhoneNumberWordDecoder.TextToNum("123-647-EYES"));
            Console.WriteLine(PhoneNumberWordDecoder.TextToNum("(325)444-TEST"));
            Console.WriteLine(PhoneNumberWordDecoder.TextToNum("653-TRY-THIS"));
            Console.WriteLine(PhoneNumberWordDecoder.TextToNum("435-224-7613"));
            Console.WriteLine(PhoneNumberWordDecoder.TextToNum("tIn-AnD-gOlD"));


            //decimal[] arr1 = { 9, 17, 30, 1.5M };
            //Console.WriteLine(Working9To5.Overtime(arr1)) ;

            //decimal[] arr2 = { 16, 18, 30, 1.8M };
            //Console.WriteLine(Working9To5.Overtime(arr2));

            //decimal[] arr3 = { 13.25M, 15, 30, 1.5M };
            //Console.WriteLine(Working9To5.Overtime(arr3));

            //Stopwatch stopwatch = new Stopwatch();
            //stopwatch.Start();
            //Console.WriteLine($"Number of palindromes: {PalindromeTimestamps.PalindromeTimestamp(2, 12, 22, 4, 35, 10)}");
            //stopwatch.Stop();
            //Console.WriteLine(stopwatch.ElapsedMilliseconds);
            //Console.WriteLine();
            //stopwatch.Restart();
            //Console.WriteLine($"Number of palindromes: {PalindromeTimestamps.PalindromeTimestamp(12, 12, 22, 13, 13, 13)}");
            //stopwatch.Stop();
            //Console.WriteLine(stopwatch.ElapsedMilliseconds);
            //Console.WriteLine();
            //stopwatch.Restart();
            //Console.WriteLine($"Number of palindromes: {PalindromeTimestamps.PalindromeTimestamp(6, 33, 15, 9, 55, 10)}");
            //stopwatch.Stop();
            //Console.WriteLine(stopwatch.ElapsedMilliseconds);
            //Console.WriteLine(ValidCreditCard.isValidCreditCard(79927398714));
            //Console.WriteLine(ValidCreditCard.isValidCreditCard(79927398713));
            //Console.WriteLine(ValidCreditCard.isValidCreditCard(709092739800713));
            //Console.WriteLine(ValidCreditCard.isValidCreditCard(1234567890123456));
            //Console.WriteLine(ValidCreditCard.isValidCreditCard(12345678901237));
            //Console.WriteLine(ValidCreditCard.isValidCreditCard(5496683867445267));
            //Console.WriteLine(ValidCreditCard.isValidCreditCard(4508793361140566));
            //Console.WriteLine(ValidCreditCard.isValidCreditCard(376785877526048));
            //Console.WriteLine(ValidCreditCard.isValidCreditCard(36717601781975));

            //Console.WriteLine(SimplifyingFractions.Simplify("4/6"));
            //Console.WriteLine(SimplifyingFractions.Simplify("10/11"));
            //Console.WriteLine(SimplifyingFractions.Simplify("100/400"));
            //Console.WriteLine(SimplifyingFractions.Simplify("16/15"));
            //Console.WriteLine(SimplifyingFractions.Simplify("16/12"));
            //Console.WriteLine(SimplifyingFractions.Simplify("8/4"));
            //Console.WriteLine(SimplifyingFractions.Simplify("0/5"));
            //Console.WriteLine(SimplifyingFractions.Simplify("5/0"));
            //Console.WriteLine(SimplifyingFractions.Simplify("42"));
        }

        static void FruitSmoothieTests()
        {
            FruitSmoothie s1 = new FruitSmoothie(new string[] { "Banana", "Soy" });

            foreach (string ingredient in s1.Ingredients)
                Console.WriteLine(ingredient);
            Console.WriteLine(s1.GetCost());  //➞ "£0.50"
            Console.WriteLine(s1.GetPrice()); //➞ "£1.25"
            Console.WriteLine(s1.GetName());


            FruitSmoothie s2 = new FruitSmoothie(new string[] { "Raspberries", "Strawberries", "Blueberries",
"Cow" });

            foreach (string ingredient in s2.Ingredients)
                Console.WriteLine(ingredient);
            Console.WriteLine(s2.GetCost());  //➞ "£3.50"
            Console.WriteLine(s2.GetPrice()); //➞ "£8.75"
            Console.WriteLine(s2.GetName());


            FruitSmoothie s3 = new FruitSmoothie(new string[] { "Blueberries", "Banana", "Oat" });

            foreach (string ingredient in s3.Ingredients)
                Console.WriteLine(ingredient);
            Console.WriteLine(s3.GetCost());  //➞ "£1.50"
            Console.WriteLine(s3.GetPrice()); //➞ "£3.75"
            Console.WriteLine(s3.GetName());
        }
        static void RemoveTheWordTests()
        {
            string[] test1 = new string[] { "s", "t", "r", "i", "n", "g", "w" };
            string[] actual1 = RemoveTheWord.RemoveWord(test1, "string");
            foreach (var letter in actual1)
            {
                Console.Write(letter);
            }
            Console.WriteLine();
            string[] test2 = new string[] { "b", "b", "l", "l", "g", "n", "o", "a", "w" };
            string[] actual2 = RemoveTheWord.RemoveWord(test2, "balloon");
            foreach (var letter in actual2)
            {
                Console.Write(letter);
            }
            Console.WriteLine();
            string[] test3 = new string[] { "a", "n", "r", "y", "o", "w" };
            string[] actual3 = RemoveTheWord.RemoveWord(test3, "norway");
            foreach (var letter in actual3)
            {
                Console.Write(letter);
            }

            Console.WriteLine();
            string[] test4 = new string[] { "t", "t", "e", "s", "t", "u" };
            string[] actual4 = RemoveTheWord.RemoveWord(test4, "testing");
            foreach (var letter in actual4)
            {
                Console.Write(letter);
            }
        }
        static void HumanReadAbleTimeTests()
        {
            Console.WriteLine(HumanReadableTime.GetReadableTime(0));
            Console.WriteLine(HumanReadableTime.GetReadableTime(5));
            Console.WriteLine(HumanReadableTime.GetReadableTime(60));
            Console.WriteLine(HumanReadableTime.GetReadableTime(86399));
            Console.WriteLine(HumanReadableTime.GetReadableTime(359999));
        }
        static void ParseltoungeTests()
        {
            //01-Parseltounge
            Console.WriteLine(Parseltounge.IsParselTounge("Sshe ssselects to eat that apple"));
            Console.WriteLine(Parseltounge.IsParselTounge("She ssselects to eat that apple"));
            Console.WriteLine(Parseltounge.IsParselTounge("You ssseldom sssspeak sso boldly, sssso messmerizingly"));
            Console.WriteLine(Parseltounge.IsParselTounge("Steve likes to eat pancakes"));
            Console.WriteLine(Parseltounge.IsParselTounge("Sssteve likess to eat pancakess"));
            Console.WriteLine(Parseltounge.IsParselTounge("Beatrice samples lemonade"));
            Console.WriteLine(Parseltounge.IsParselTounge("Beatrice enjoysss lemonade"));
        }
        static void FizzBuzzTests()
        {
            // 00-FizzBuzz
            FizzBuzz myFizzBuzz = new FizzBuzz();

            string[] fizzBuzzStrings = myFizzBuzz.FizzBuzzMain(15);

            foreach (string item in fizzBuzzStrings)
            {
                Console.WriteLine(item);
            }
        }
    }
}

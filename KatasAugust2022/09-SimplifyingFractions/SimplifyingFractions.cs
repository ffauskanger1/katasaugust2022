﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatasAugust2022._09_SimplifyingFractions
{
    static public class SimplifyingFractions
    {
        public static string Simplify(string fraction)
        {

            // Fix bugs
            string result = "Error!";
            string[] splitString = fraction.Split("/");

            if (splitString[0] == "0")
                return "0";

            if (splitString.Length != 2 || splitString[1] == "0")
                return result;

            int numerator = int.Parse(splitString[0]);
            int denominator = int.Parse(splitString[1]);

            int largestDenominator = denominator;
            int tempNumerator = numerator;

            while (largestDenominator != tempNumerator)
            {
                if (tempNumerator > largestDenominator)
                    tempNumerator -= largestDenominator;
                else
                    largestDenominator -= tempNumerator;
            }

            if(denominator/largestDenominator == 1 || numerator == 0)
                return $"{numerator/largestDenominator}";


            return $"{numerator/largestDenominator}/{denominator/largestDenominator}";
        }
    }
}

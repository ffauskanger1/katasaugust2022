using KatasAugust2022._04_BriefcaseLock;
using System;
using Xunit;

namespace KatasAugust2022Tests
{
    public class BriefcaseLockTests
    {
        [Fact]
        public void MinTurns_Code4089ShortestTurns_ShouldReturnCorrectSum()
        {
            // Arrange
            int expected = 9;
            string code = "4089";
            string target = "5672";

            // Act 

            int actual = BriefcaseLock.MinTurns(code, target);

            // Assert 

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void MinTurns_Code1111ShortestTurns_ShouldReturnCorrectSum()
        {
            // Arrange
            int expected = 2;
            string code = "1111";
            string target = "1100";

            // Act 

            int actual = BriefcaseLock.MinTurns(code, target);

            // Assert 

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void MinTurns_Code2391ShortestTurns_ShouldReturnCorrectSum()
        {
            // Arrange
            int expected = 10;
            string code = "2391";
            string target = "4984";

            // Act 

            int actual = BriefcaseLock.MinTurns(code, target);

            // Assert 

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void MinTurns_Code0000ShortestTurns_ShouldReturnCorrectSum()
        {
            // Arrange
            int expected = 10;
            string code = "0000";
            string target = "1234";

            // Act 

            int actual = BriefcaseLock.MinTurns(code, target);

            // Assert 

            Assert.Equal(expected, actual);
        }
    }
}

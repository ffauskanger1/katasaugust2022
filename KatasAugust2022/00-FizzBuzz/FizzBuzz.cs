﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatasAugust2022._00_FizzBuzz
{
    public class FizzBuzz
    {
        public string[] FizzBuzzMain(int number)
        {

            string[] fizzBuzzStrings = new string[number];  

            for (int i = 0; i < number; i++)
            {
                fizzBuzzStrings[i] = FizzBuzzChecker(i + 1);
            }
            return fizzBuzzStrings;
        }

        private string FizzBuzzChecker(int num)
        {
            if (num % 15 == 0) 
            {
                return "Fizz";
            }
            
            if (num % 5 == 0)
            {
                return "Buzz";
            }
            
            if (num % 3 == 0)
            {
                return "FizzBuzz";
            }

            return num.ToString();
        }

    }
}

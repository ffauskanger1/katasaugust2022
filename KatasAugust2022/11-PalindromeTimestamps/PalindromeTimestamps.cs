﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatasAugust2022._11_PalindromeTimestamps
{
    public class PalindromeTimestamps
    {
        public static int PalindromeTimestamp(int startHour, int startMinutes, int startSeconds, int endHour, int EndMinutes, int endSeconds)
        {
            int palindromes = 0;
            while(startHour != endHour || startMinutes != EndMinutes || startSeconds != endSeconds)
            {
                if (startSeconds >= 60)
                {
                    startMinutes++;
                    startSeconds = 0;
                }
                    
                if (startMinutes >= 60)
                {
                    startHour++;
                    startMinutes = 0;
                }

                if(startHour >= 24)
                {
                    startHour = 0;
                }

                string startString = $"{startHour:00}:{startMinutes:00}:{startSeconds:00}";
                char[] charArray = startString.ToCharArray();
                Array.Reverse(charArray);
                string reversedString = new string(charArray);

                if (startString == reversedString)
                {
                    Console.WriteLine($"Palindrome at: {startString}");
                    palindromes++;
                }
               startSeconds++;
            }

            return palindromes;
        }
    }
}

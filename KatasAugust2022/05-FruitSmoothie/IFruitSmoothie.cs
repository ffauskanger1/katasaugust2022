﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatasAugust2022._05_FruitSmoothie
{
    public interface IFruitSmoothie
    {
        public string[] Ingredients { get; set; }
        public string GetCost();
        public string GetPrice();
        public string GetName();
    }
}

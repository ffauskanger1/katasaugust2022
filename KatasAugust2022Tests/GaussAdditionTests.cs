﻿using KatasAugust2022._05_FruitSmoothie;
using KatasAugust2022._07_GaussAddition;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace KatasAugust2022Tests
{
    public class GaussAdditionTests
    {

        [Fact]
        public void Gauss_GaussAddition100_ShouldResult5050()
        {
            // Arrange

            int expected = 5050;
            // Act 

            int actual = GaussAddition.Gauss(100);

            // Assert 

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Gauss_GaussAddition5001And7000_ShouldResult120010000()
        {
            // Arrange

            int expected = 12001000;
            // Act 

            int actual = GaussAddition.Gauss(5001, 7000);

            // Assert 

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Gauss_GaussAddition1975And165_ShouldResult1937770()
        {
            // Arrange

            int expected = 1937770;
            // Act 

            int actual = GaussAddition.Gauss(1975, 165);

            // Assert 

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Gauss_GaussAddition5And5_ShouldResult5()
        {
            // Arrange

            int expected = 5;
            // Act 

            int actual = GaussAddition.Gauss(5, 5);

            // Assert 

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Gauss_GaussAddition0_ShouldResult0()
        {
            // Arrange

            int expected = 0;
            // Act 

            int actual = GaussAddition.Gauss(0);

            // Assert 

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Gauss_GaussAddition1And0_ShouldResult1()
        {
            // Arrange

            int expected = 1;
            // Act 

            int actual = GaussAddition.Gauss(1,0);

            // Assert 

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Gauss_GaussAdditionMinus5And10_ShouldResult40()
        {
            // Arrange

            int expected = 40;
            // Act 

            int actual = GaussAddition.Gauss(-5, 10);

            // Assert 

            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Gauss_GaussAdditionMinus10And5_ShouldResultMinus40()
        {
            // Arrange

            int expected = -40;
            // Act 

            int actual = GaussAddition.Gauss(-10, 5);

            // Assert 

            Assert.Equal(expected, actual);
        }
    }
}
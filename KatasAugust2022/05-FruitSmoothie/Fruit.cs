﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatasAugust2022._05_FruitSmoothie
{
    public static class Fruit
    {
        public static Dictionary<string, double> Fruits { get; set; }

        public static Dictionary<string, double> GetFruits()
        {
            Fruits = new Dictionary<string, double>();
            Fruits.Add("Strawberries", 1.5);
            Fruits.Add("Banana", 0.5);
            Fruits.Add("Mango", 2.5);
            Fruits.Add("Blueberries", 1.0);
            Fruits.Add("Raspberries", 1.0);
            Fruits.Add("Apple", 1.75);
            Fruits.Add("Pineapple", 3.50);

            return Fruits;
        }
    }
}

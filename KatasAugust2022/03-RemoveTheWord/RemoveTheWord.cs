﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatasAugust2022._03_RemoveTheWord
{
    public static class RemoveTheWord
    {
        public static string[] RemoveWord(string[] keepString, string removeString)
        {
            List<string> lettersLeft = keepString.ToList<string>();
            string[] stringsToRemove = removeString.Select(s => s.ToString()).ToArray();

            foreach (string c in stringsToRemove)
            {
                if(keepString.Contains(c))
                {
                    lettersLeft.Remove(c);
                }
            }

            return lettersLeft.ToArray();
        }
    }
}


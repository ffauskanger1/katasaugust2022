﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatasAugust2022._07_GaussAddition
{
    public static class GaussAddition
    {
        public static int Gauss(int n) => (n * (n + 1)) / 2;
        public static int Gauss(int n, int m) => ((n+m) * (Math.Abs(n-m)+1)) / 2;
    }
}
